#include "ofApp.h"
#include <cv.h>

ofApp::ofApp()
{

}

ofApp::~ofApp()
{
    delete [] backgroundMeanBuffer;
    delete [] backgroundVarianceBuffer;
}

void ofApp::sampleBackground()
{
    mBackgroundImage.setFromPixels(mGrayImage.getPixels(), mVideoGrabber.getWidth(), mVideoGrabber.getHeight());
}

//--------------------------------------------------------------
void ofApp::setup(){
    mVideoGrabber.setVerbose(true);
    mVideoGrabber.initGrabber(320, 240); //1280×720

    mHaarFinder.setup("haarcascade_eye.xml");

    int width = mVideoGrabber.getWidth(), height = mVideoGrabber.getHeight();

    mRenderedFirstFrame = false;

    backgroundMeanBuffer = new float[width * height];
    backgroundVarianceBuffer = new float[width * height];
}

//--------------------------------------------------------------
void ofApp::update(){
    mVideoGrabber.update();

    int width = mVideoGrabber.getWidth(), height = mVideoGrabber.getHeight();

    mColorImage.setFromPixels(mVideoGrabber.getPixels(), mVideoGrabber.getWidth(), mVideoGrabber.getHeight());
    mGrayImage.setFromColorImage(mColorImage);

    //BUFFER MODE:
    if(!mRenderedFirstFrame)
    {
        mRenderedFirstFrame = true;
        mMaskedImage.setFromColorImage(mColorImage);

        for(int i=0; i<height; ++i){
            for(int j=0; j<width; ++j){
                int idx = i * width + j;
                backgroundMeanBuffer[idx] = mGrayImage.getPixels().getColor(idx).r;
                backgroundVarianceBuffer[idx] = 1;
            }
        }

        sampleBackground();
    }

    //recalculate mean and variance per pixel:
    float rho = .01;
    float kThreshold = 2;//2.5;
    for(int i=0; i<height; ++i){
        for(int j=0; j<width; ++j){
            int idx = i * width + j;

            float value = mGrayImage.getPixels().getColor(idx).r;

            float mean = rho * value + (1 - rho) * backgroundMeanBuffer[idx];
            float d = abs(value - mean);
            float variance = d * d * rho + (1 - rho) *  backgroundVarianceBuffer[idx];
            float deviation = sqrt(variance);

            ofColor color(0,0,0);
            if(abs(value - mean) / deviation > kThreshold)
                color.set(value,value,value);

            mMaskedImage.getPixels().setColor(idx, color);

            backgroundMeanBuffer[idx] = mean;
            backgroundVarianceBuffer[idx] = variance;
        }
    }

    mMaskedImage.setFromPixels(mMaskedImage.getPixels(), width, height);

    //mHaarFinder.findHaarObjects(mGrayImage);
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetColor(255, 255, 255);

    mVideoGrabber.draw(0, 0);
    mGrayImage.draw(mVideoGrabber.getWidth(), 0);
    mMaskedImage.draw(2 * mVideoGrabber.getWidth(), 0);

    mMaskedImage.threshold(1);
    mMaskedImage.draw(0, mVideoGrabber.getHeight());

    mMaskedImage.absDiff(mGrayImage, mBackgroundImage);//.setFromPixels(mMaskedImage.getPixels());
    mMaskedImage.convertToRange(0, 255);
    mMaskedImage.threshold(40);
    mMaskedImage.draw(mVideoGrabber.getWidth(), mVideoGrabber.getHeight());

//adaptiveThreshold

    mContourFinder.findContours(mMaskedImage, 1000, (340*240), 100, false, true);
    mContourFinder.draw(2 * mVideoGrabber.getWidth(), mVideoGrabber.getHeight());

/*
    mMaskedImage.setFromPixels(mGrayImage.getPixels());
    mMaskedImage.adaptiveThreshold(10);
    mMaskedImage.draw(2 * mVideoGrabber.getWidth(), mVideoGrabber.getHeight());
*/
    /*
    //HAAR DETECTION:
    for(int i = 0; i < mHaarFinder.blobs.size(); i++){
        ofDrawRectangle( mHaarFinder.blobs[i].boundingRect );
    }
    */
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    if(key == ' ') {
        sampleBackground();
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
